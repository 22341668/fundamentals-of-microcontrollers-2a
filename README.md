# FUNDAMENTALS OF MICROCONTROLLERS 2A

## Introduction

This module will approach the subject of digital systems from two angles. Firstly, the topic of programmable logic devices (PLDs) covered in Digital Electronics 1B would be extended to a higher and application-based level. Secondly, the students would be introduced to the concept of embedded microcontroller programming.