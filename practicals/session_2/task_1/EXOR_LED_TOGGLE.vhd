library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- declare entity
entity EXOR_LED_TOGGLE is
	port (
		-- INPUTS
		switches	:	in  std_logic_vector(2 downto 0);
		
		-- OUTPUTS
		leds 		:	out std_logic_vector(7 downto 0)
	);	
end EXOR_LED_TOGGLE;

-- declare architecture
architecture behavioral of EXOR_LED_TOGGLE is
begin
	process(switches)
		begin
		
		-- Switch off all LEDs	
		leds <= "11111111";
		
		-- sequential logic
		if switches = "000" then 
			leds(1 downto 0) <= "10";
		
		else
			leds(1 downto 0) <= "01";
		
		end if;
	end process;
end behavioral;