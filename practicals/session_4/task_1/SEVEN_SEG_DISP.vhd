library ieee;
use ieee.std_logic_1164.all;


entity SEVEN_SEG_DISP is
	port (
		switches : in std_logiC_vector(3 downto 0);
		led_grid : out std_logiC_vector(7 downto 0);
		seg_leds : out std_logiC_vector(7 downto 1);
		sel		: out std_logiC_vector(1 downto 0)
	);
end SEVEN_SEG_DISP;

architecture SEVEN_SEG_DISP_ARCH of SEVEN_SEG_DISP is
signal ctrl_signal: std_logiC_vector(3 downto 0);
begin
	-- initial conditions
	sel(0) 	<= '0';
	sel(1) 	<= '1';
	led_grid <= "11111111";
	
	ctrl_signal <= switches;
	with ctrl_signal select 
		seg_leds(7 downto 1) <= "1111110" when "0000", -- 0
										"0110000" when "0001", -- 1
										"1101101" when "0010", -- 2
										"1111001" when "0011", -- 3
										"0110011" when "0100", -- 4
										"1011011" when "0101", -- 5
										"1011111" when "0110", -- 6
										"1110000" when "0111", -- 7
										"1111111" when "1000", -- 8
										"1100111" when "1001", -- 9
										"0011100" when others;
end SEVEN_SEG_DISP_ARCH;